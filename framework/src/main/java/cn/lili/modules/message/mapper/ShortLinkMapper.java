package cn.lili.modules.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.lili.modules.message.entity.ShortLink;

import java.util.List;

/**
 * 短链接 Dao层
 * @author Chopper
 */
public interface ShortLinkMapper extends BaseMapper<ShortLink> {

}